AQS 2.0
=======

Try:

```sh-session
$ make
[ ... ]
cassandra_1  | INFO  [main] 2021-08-11 00:07:25,496 Gossiper.java:1832 - Waiting for gossip to settle...
cassandra_1  | INFO  [main] 2021-08-11 00:07:33,498 Gossiper.java:1863 - No gossip backlog; proceeding
cassandra_1  | INFO  [main] 2021-08-11 00:07:33,751 NativeTransportService.java:68 - Netty using native Epoll event loop
cassandra_1  | INFO  [main] 2021-08-11 00:07:33,799 Server.java:158 - Using Netty Version: [netty-buffer=netty-buffer-4.0.44.Final.452812a, netty-codec=netty-codec-4.0.44.Final.452812a, netty-codec-haproxy=netty-codec-haproxy-4.0.44.Final.452812a, netty-codec-http=netty-codec-http-4.0.44.Final.452812a, netty-codec-socks=netty-codec-socks-4.0.44.Final.452812a, netty-common=netty-common-4.0.44.Final.452812a, netty-handler=netty-handler-4.0.44.Final.452812a, netty-tcnative=netty-tcnative-1.1.33.Fork26.142ecbb, netty-transport=netty-transport-4.0.44.Final.452812a, netty-transport-native-epoll=netty-transport-native-epoll-4.0.44.Final.452812a, netty-transport-rxtx=netty-transport-rxtx-4.0.44.Final.452812a, netty-transport-sctp=netty-transport-sctp-4.0.44.Final.452812a, netty-transport-udt=netty-transport-udt-4.0.44.Final.452812a]
cassandra_1  | INFO  [main] 2021-08-11 00:07:33,799 Server.java:159 - Starting listening for CQL clients on /0.0.0.0:9042 (unencrypted)...
cassandra_1  | INFO  [main] 2021-08-11 00:07:33,828 CassandraDaemon.java:564 - Not starting RPC server as requested. Use JMX (StorageService->startRPCServer()) or nodetool (enablethrift) to start it
cassandra_1  | INFO  [main] 2021-08-11 00:07:33,828 CassandraDaemon.java:650 - Startup complete
```

Wait until you see the 'Startup complete' log message, then in another terminal:

```sh-session
$ make bootstrap
[ ... ]
```

## TODO

- [x] Implement the `load` target (`Makefile`) to add Cassandra sample data
- [ ] Extend the environment to include Druid (+ sample data)
